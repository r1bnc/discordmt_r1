--[[
    Modified Discord Bridge (discordmt)
    Copyright (C) 2022  R1BNC
    Credits to the original discordmt project, chat_filter

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]--
local http = minetest.request_http_api()
local settings = minetest.settings

local host = settings:get('discord.host') or 'localhost'
local port = settings:get('discord.port') or 8080
local timeout = 10

local function check_chat(message)
    
    local filter = {
      --English
      "fuck",
      "bitch",
      "shit",
      "shitstorm",
      "shitstain",
      "dick",
      "cock",
      "nigger",
      "faggot",
      "gay",
      "whore",
      "incel",
      "cuck",
      "fucker",
      "motherfucker",
      "retard",
      "penis",
      "vagina",
      "vag",
      "penile",
      "autistic",
      "autist",
      "pussy",
      "masturbate",
      "sex",
      "intercourse",
      "asshole",
      "asswhipe",
      "cocksucker",
      "bitchass",
      "titties",
      "tits",
      "boobs",
      "breast",
      "breasts",
      "degenerate",
      "bastard",
  
      --Russian
      "сука",
      "блять",
      "бля",
      "мля",
      "блин",
      "твою мать",
      "твою же мать",
      "дибил",
      "аут",
      "аутист",
      "манда",
      "ебать",
      "ебало закрой",
      "заебал",
      "отвали",
      "заткнись",
      "мудак",
      "хуй",
      "охуел?",
      "это пиздец",
      "пизда",
      "сволочь",
      "жопа",
      "гавно",
      "лох",
      "гандон",
      "ублюдок",
      "срать",
      "мне насрать",
      "мне похуй",
      "черт",
      "трахнуть",
      "трахнул",
      "дегенерат",
      "хрен",
      "хуй",
      "дерьмо",
      "пошел к чорту",
      "мне плевать",
      "херня",
      "хрень",
      "один хрен",
      "ни хрена",
      "ну его нахрен",
      "иди нахер",
      "нахрен",
      "пошёл",
      "нахер",
      "нахрен",
  
      --German
      "arschloch",
      "arsch",
      "schwanz",
      "wichser",
      "schlampe",
      "hurre",
      "fick dich",
      "mutterficker",
      "hurensohn",
      "mastubiren",
      "scheide",
      "geschlechtsverkehr",
      "fotze",
      "missgeburt",
      "missgeburt",
      "vollidiot",
      "brüste",
      "titten",
      "scheiße"
    }
    local clean = true
    --Verify if message contains any of the following disallowed words
    for k,v in pairs(filter) do
      local disallowed_word = v
      local to_verify = string.lower(message)
  
      if string.match(to_verify, disallowed_word) then
        clean = false
        break
      end
  
    end
  
    --return boolean
    return clean
  end

discord = {}
discord.text_colorization = settings:get('discord.text_color') or '#ffffff'

discord.registered_on_messages = {}

local irc_enabled = minetest.get_modpath("irc")

discord.register_on_message = function(func)
    table.insert(discord.registered_on_messages, func)
end   

discord.chat_send_all = minetest.chat_send_all

discord.handle_response = function(response)
    local data = response.data
    if data == '' or data == nil then
        return
    end
    local data = minetest.parse_json(response.data)
    if not data then
        return
    end
    if data.messages then
        for _, message in pairs(data.messages) do
            for _, func in pairs(discord.registered_on_messages) do
                func(message.author, message.content)
            end
            if not check_chat(message.content) then
                minetest.log('action', '[discordmt] BlockedWord Player: '..message.author..' from saying ' ..message.content)
                message.content = "*Message blocked for violation of rule.*"
            end
            local msg = ('<%s@Discord> %s'):format(message.author, message.content)
            discord.chat_send_all(minetest.colorize(discord.text_colorization, msg))
            if irc_enabled then
                irc.say(msg)
            end
        end
    end
    if data.commands then
        local commands = minetest.registered_chatcommands
        for _, v in pairs(data.commands) do
            if commands[v.command] then
                if minetest.get_ban_description(v.name) ~= '' then
                    discord.send('You cannot run commands because you are banned.', v.context or nil)
                    return
                end
                -- Check player privileges
                local required_privs = commands[v.command].privs or {}
                local player_privs = minetest.get_player_privs(v.name)
                for priv, value in pairs(required_privs) do
                    if player_privs[priv] ~= value then
                        discord.send('Insufficient privileges.', v.context or nil)
                        return
                    end
                end
                local old_chat_send_player = minetest.chat_send_player
                minetest.chat_send_player = function(name, message)
                    if not check_chat(message) then
                        minetest.log('action', '[discordmt] BlockedWord Player: '..name..' from saying ' ..message)
                        message = "*Message blocked for violation of rule.*"
                    end
                    old_chat_send_player(name, message)
                    if name == v.name then
                        discord.send(message, v.context or nil)
                    end
                end
                success, ret_val = commands[v.command].func(v.name, v.params or '')
                if ret_val then
                    discord.send(ret_val, v.context or nil)
                end
                minetest.chat_send_player = old_chat_send_player
            else
                discord.send(('Command not found: `%s`'):format(v.command), v.context or nil)
            end
        end
    end
    if data.logins then
        local auth = minetest.get_auth_handler()
        for _, v in pairs(data.logins) do
            local authdata = auth.get_auth(v.username)
            local result = false
            if authdata then
                result = minetest.check_password_entry(v.username, authdata.password, v.password)
            end
            -- if login ok  but check privs, deny
            local request = {
                type = 'DISCORD_LOGIN_RESULT',
                user_id = v.user_id,
                username = v.username,
                success = result
            }
            http.fetch({
                url = tostring(host)..':'..tostring(port),
                timeout = timeout,
                post_data = minetest.write_json(request)
            }, discord.handle_response)
        end
    end
end

discord.send = function(message, id)
    local data = {
        type = 'DISCORD-RELAY-MESSAGE',
        content = minetest.strip_colors(message)
    }
    if id then
        data['context'] = id
    end
    http.fetch({
        url = tostring(host)..':'..tostring(port),
        timeout = timeout,
        post_data = minetest.write_json(data)
    }, function(_) end)
end

minetest.chat_send_all = function(message)
    if not check_chat(message)  then
        minetest.log('action', '[discordmt] BlockedWord Msg: '..message)
        message = "[Server] *Message blocked for violation of rule.*"
    end
    discord.chat_send_all(message)
    discord.send(message)
end

minetest.register_on_chat_message(function(name, message)
    if check_chat(message) then
        if message:sub(1, 1) == "/" then
            print(name .. " ran chat command")
        elseif minetest.check_player_privs(name, { shout = true }) then
            if not check_chat(message)  then
                minetest.log('action', '[discordmt] BlockedWord Player: '..name..' from saying ' ..message)
                minetest.chat_send_player(name, minetest.colorize("#ff0000" ,"Your message has been blocked!"))
                message = "*Message blocked for violation of rule.*"
            end
                discord.send(('<%s> %s'):format(name, message))
        else
            minetest.log('action', '[discordmt] BlockedNoShout Player: '..name..' from saying ' ..message)
            return true
        end
    else
        minetest.log('action', '[discordmt] BlockedWord Player: '..name..' from saying ' ..message)
        minetest.chat_send_player(name, minetest.colorize("#ff0000" ,"Your message has been blocked!"))
        message = "*Message blocked for violation of rule.*"
        discord.send(('<%s> %s'):format(name, message))
        return true
end
    return false
end)

local timer = 0
minetest.register_globalstep(function(dtime)
    if dtime then
        timer = timer + dtime
        if timer > 0.2 then
            http.fetch({
                url = tostring(host)..':'..tostring(port),
                timeout = timeout,
                post_data = minetest.write_json({
                    type = 'DISCORD-REQUEST-DATA'
                })
            }, discord.handle_response)
            timer = 0
        end
    end
end)

minetest.register_on_shutdown(function()
    discord.send('*** Server shutting down...')
end)

if irc_enabled then
    discord.old_irc_sendLocal = irc.sendLocal
    irc.sendLocal = function(msg)
        if not check_chat(msg)  then
            minetest.log('action', '[discordmt] Blocked Message: ' ..msg)
            message = "*Message blocked for violation of rule.*"
        end
        discord.old_irc_sendLocal(msg)
        discord.send(msg)
    end
end

discord.send('*** Server started!')

