#!/usr/bin/env python3
"""
Credits to original authors, modifications
Copyright (C) 2022 R1BNC

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import sys
from aiohttp import web
import aiohttp
import discord
from discord.ext import commands
import asyncio
import json
import time
import configparser
import re

from discord_webhook import DiscordWebhook
from profanity_check import predict_prob

import logging
logging.basicConfig(level=logging.WARN,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

rate_limit_message = "INFORMATION: RATE LIMIT WAS HIT"
# Seconds to wait before generating request (in seconds)
timeout = 5
# Minimum length to count as caps
minLen = 5

allowed_mentions = {
    "parse": []
}


Badpatterns = [

    'stfu',
    'nigga',
    'hentai',
    'pussy',
    'prostate',

    # Tagalog/Bisaya
    'puta',
    'tangina',
    'yawa',
    'gagi',
    'ulol',

    # Spanish
    'mierda',
    'chupa',
    'bastordo',
    'carajo',
    'coger',
    'idiota',
    'estupido',
    'imbécil',
    'pendejo',
    'zorra',
    'maricon',
    'mamón',
    'tarado',

    # English
    'fuc',
    'fuck',
    'bitch',
    'shit',
    'shitstorm',
    'shitstain',
    'dick',
    'cock',
    'nigger',
    'faggot',
    'whore',
    'incel',
    'cuck',
    'fucker',
    'motherfucker',
    'retard',
    'penis',
    'vagina',
    'penile',
    'pussy',
    'masturbate',
    'sex',
    'intercourse',
    'asshole',
    'asswhipe',
    'cocksucker',
    'bitchass',
    'titties',
    'boobs',
    'breast',
    'breasts',
    'bastard',

    # Russian
    'сука',
    'блять',
    'бля',
    'мля',
    'блин',
    'твою мать',
    'твою же мать',
    'дибил',
    'аут',
    'аутист',
    'манда',
    'ебать',
    'ебало закрой',
    'заебал',
    'отвали',
    'заткнись',
    'мудак',
    'хуй',
    'охуел?',
    'это пиздец',
    'пизда',
    'сволочь',
    'жопа',
    'гавно',
    'лох',
    'гандон',
    'ублюдок',
    'срать',
    'мне насрать',
    'мне похуй',
    'черт',
    'трахнуть',
    'трахнул',
    'дегенерат',
    'хрен',
    'хуй',
    'дерьмо',
    'пошел к чорту',
    'мне плевать',
    'херня',
    'хрень',
    'один хрен',
    'ни хрена',
    'ну его нахрен',
    'иди нахер',
    'нахрен',
    'пошёл',
    'нахер',
    'нахрен',

    # German
    'arschloch',
    'arsch',
    'schwanz',
    'wichser',
    'schlampe',
    'hurre',
    'fick dich',
    'mutterficker',
    'hurensohn',
    'mastubiren',
    'scheide',
    'geschlechtsverkehr',
    'fotze',
    'missgeburt',
    'missgeburt',
    'vollidiot',
    'brüste',
    'titten',
    'scheiße'
    ]


DatingPatterns = ['babe', 'husband', 'wife', 'kiss',
                  'marry', 'love', 'baby', 'novio', 'novia', 'beso', 'mama']


config = configparser.ConfigParser()

config.read('relay.conf')


async def transmit_webhook(chat_message, webhookurl, timeout):
    # Upload code
    try:

        # Upload data
        webhook = DiscordWebhook(
            url=webhookurl, timeout=timeout, allowed_mentions=allowed_mentions)
        webhook.set_content(chat_message)
        webhook_execute = webhook.execute()
        # Credits: https://github.com/novasz @ https://github.com/lovvskillz/python-discord-webhook/issues/47
        # Check if webhook response is 200/204
        if webhook_execute.status_code in [200, 204]:
            logger.debug("[discordmt] Uploaded: [" + chat_message + "]\n")
        # We are being rate limited :(
        elif webhook_execute.status_code == 429:
            errors = json.loads(webhook_execute.content.decode('utf-8'))
            wh_sleep = (int(errors['retry_after']) / 1000) + 0.15
            logger.warning("[discordmt] Webhook rate limited: sleeping for " + str(wh_sleep) + " seconds...\n")
            # Sleep
            await asyncio.sleep(wh_sleep)
            # Upload rate limit hit message, discard other messages
            webhook = DiscordWebhook(
                url=webhookurl, timeout=timeout, allowed_mentions=allowed_mentions)
            webhook.set_content(rate_limit_message)
            webhook_execute = webhook.execute()
            if webhook_execute.status_code in [200, 204]:
                logger.warning("[discordmt] Notified Ratelimit was hit\n")
            elif webhook_execute.status_code == 429:
                logger.warning( "[discordmt] Can't send rate limit notification because we are rate limited again!\n")
            else:
                logger.info("[discordmt] Unknown Code: " +
                            str(webhook_execute.status_code))
        else:
            logger.info("[discordmt] Unknown Code: " + str(webhook_execute.status_code))
    except Exception as ex:
        logger.warning("[discordmt] Exception Occurred @ transmit_webhook - Message: " + str(ex) + "\n")


async def ul_caps(chat_message):
    # Caps Detected
    logging.debug("**Caps**: `" + chat_message +
                 "`")
    await transmit_to_filtershook("**Caps**: `" + chat_message + "`")

async def transmit_to_bothook(message):
    for webhook_bot in records_url:
        await transmit_webhook(message, webhook_bot, timeout)
    return

async def transmit_to_filtershook(message):
    for webhook_bot in filters_url:
        await transmit_webhook(message, webhook_bot, timeout)
    return

# Python3 code to remove whitespace
async def remove(string):
    return "".join(string.split())

async def get_violation(chat_message):

    probab = predict_prob([chat_message])
    # >= 80% Profanity
    if probab >= 0.80:

        percent = f"{float(probab):.0%}"

        logging.debug("**Badword: "+ percent + "**: `" + chat_message +
              "`")

        await transmit_to_filtershook("**Badword: "+ percent + "**: `"  + chat_message + "`")

    else:
        # < 80% Profanity
        BadWordList = re.compile('|'.join(Badpatterns))
        if bool(re.search(BadWordList, chat_message)):
            # Badword Detected
            if not (re.search('has left the server',chat_message)):
                logging.debug("**Filtered**: `" + chat_message +
                      "`")
                await transmit_to_filtershook("**Badword**: `" + chat_message + "`")

    # Dating/Roleplaying Detection
    DatingWordList = re.compile('|'.join(DatingPatterns))
    if bool(re.search(DatingWordList, chat_message)):

        # Dating Detected
        if not (re.search('has joined the server',chat_message) or not re.search('has left the server',chat_message)):
            logging.debug("**Dating/RP**: `" + chat_message +
                  "`")
            await transmit_to_filtershook("**Dating/RP**: `" + chat_message + "`")

    # Check if CAPITALS
    count = 0
    for i in chat_message:
        if i.isupper():
            count=count+1
    # Calculate Caps Threshold
    if (count > 0) and (count >= len(chat_message)/2) and not bool(re.search("joined the game",chat_message)) and not bool(re.search("left the game", chat_message)):
        count = 0

        if len(chat_message) > minLen:
            await ul_caps(chat_message)

    # Literal ALL CAPS MESSAGE
    elif chat_message.isupper():
        count = 0

        if len(chat_message) > minLen:
            await ul_caps(chat_message)


async def check_violation(chat_message):

    probab = predict_prob([chat_message.lower()])
    # >= 80% Profanity
    if probab >= 0.80:

        percent = f"{float(probab):.0%}"

        logging.debug("**Badword: "+ percent + "**: `" + chat_message +
              "`")
        return True

    else:
        # < 80% Profanity
        BadWordList = re.compile('|'.join(Badpatterns))
        if bool(re.search(BadWordList, chat_message.lower())):
            # Badword Detected
            if not (re.search('has left the server',chat_message)):
                logging.debug("**Filtered**: `" + chat_message +
                      "`")
                return True

    # Dating/Roleplaying Detection
    DatingWordList = re.compile('|'.join(DatingPatterns))
    if bool(re.search(DatingWordList, chat_message.lower())):

        # Dating Detected
        if not (re.search('has joined the server',chat_message) or not re.search('has left the server',chat_message)):
            logging.debug("**Dating/RP**: `" + chat_message +
                  "`")
            return True

    # Check if CAPITALS
    count = 0
    for i in chat_message:
        if i.isupper():
            count=count+1
    # Calculate Caps Threshold
    if (count > 0) and (count >= len(chat_message)/2) and not bool(re.search("joined the game",chat_message)) and not bool(re.search("left the game", chat_message)):
        count = 0

        if len(chat_message) > minLen:
            return True

    # Literal ALL CAPS MESSAGE
    elif chat_message.isupper():
        count = 0

        if len(chat_message) > minLen:
            return True

class Queue():
    def __init__(self):
        self.queue = []
    def add(self, item):
        self.queue.append(item)
    def get(self):
        if len(self.queue) >=1:
            item = self.queue[0]
            del self.queue[0]
            return item
        else:
            return None
    def get_all(self):
        items = self.queue
        self.queue = []
        return items
    def isEmpty(self):
        return len(self.queue) == 0

def clean_invites(string):
    return ' '.join([word for word in string.split() if not ('discord.gg' in word) and not ('discordapp.com/invite' in word)])

outgoing_msgs = Queue()
command_queue = Queue()
login_queue = Queue()

prefix = config['BOT']['command_prefix']

bot = commands.Bot(command_prefix=prefix)

channel_id = int(config['RELAY']['channel_id'])

connected = False

listen = str(config['RELAY']['listen'])
port = int(config['RELAY']['port'])
token = config['BOT']['token']
logins_allowed = True if config['RELAY']['allow_logins'] == 'true' else False
do_clean_invites = True if config['RELAY']['clean_invites'] == 'true' else False
do_use_nicknames = True if config['RELAY']['use_nicknames'] == 'true' else False

records_url = str(config['BOT']['records_url']).split(',')
filters_url = str(config['BOT']['filters_url']).split(',')

last_request = 0

channel = None
authenticated_users = {}

def check_timeout():
    return time.time() - last_request <= 1

async def get_or_fetch_channel(id):
    target_channel = bot.get_channel(id)
    if target_channel is None:
        target_channel = await bot.fetch_channel(id)
        if target_channel is None:
            print(f'Failed to fetch channel {id!r}.')

    return target_channel

async def get_or_fetch_user(user_id):
    user = bot.get_user(user_id)
    if user is None:
        user = await bot.fetch_user(user_id)
        if user is None:
            print(f'Failed to fetch user {user_id!r}.')

    return user

async def handle(request):
    global last_request
    last_request = time.time()
    text = await request.text()
    try:
        data = json.loads(text)
        if data['type'] == 'DISCORD-RELAY-MESSAGE':
            msg = discord.utils.escape_mentions(data['content'])[0:2000]
            r = re.compile(r'\x1b(T|F|E|\(T@[^\)]*\))')
            msg = r.sub('', msg)
            if 'context' in data.keys():
                id = int(data['context'])
                target_channel = await get_or_fetch_channel(id)
                if target_channel is not None:
                    if await check_violation(msg): # TODO: Kick/Mute Player?
                        logger.warning("[discordmt] Message blocked from Minetest: **" + str(msg) + "**")
                        await transmit_to_bothook("[discordmt] Message blocked from Minetest: " + str(msg))
                    else:
                        await target_channel.send(msg)
                        
            else:
                if await check_violation(msg): # TODO: Kick/Mute Player?
                    logger.warning("[discordmt] Message blocked from Minetest: **" + str(msg) + "**")
                    await transmit_to_bothook("[discordmt] Message blocked from Minetest: " + str(msg))
                else:
                    await channel.send(msg)
            return web.Response(text = 'Acknowledged') # discord.send should NOT block extensively on the Lua side

        if data['type'] == 'DISCORD_LOGIN_RESULT':
            user_id = int(data['user_id'])
            user = await get_or_fetch_user(user_id)
            if user is not None:
                if data['success'] is True:
                    authenticated_users[user_id] = data['username']
                    logger.warning("[discordmt] Login ok for UID:" + str(user_id) + " - Discord User:" + str(user) )
                    await transmit_to_bothook("[discordmt] **Discord Bridge Login Successful**: UID:`" + str(user_id) + "` - Discord User: " + str(user))
                    await user.send('[discordmt] Login successful.')
                else:
                    logger.warning("[discordmt] Login Fail")
                    await transmit_to_bothook("[discordmt] **Discord Bridge Login Failed**: UID:`" + str(user_id) + "` - Discord User: " + str(user))
                    await user.send('[discordmt] Login failed.')
    except:
        pass
    response = json.dumps({
        'messages' : outgoing_msgs.get_all(),
        'commands' : command_queue.get_all(),
        'logins' : login_queue.get_all()
    })
    return web.Response(text = response)
    

app = web.Application()
app.add_routes([web.get('/', handle),
                web.post('/', handle)])

@bot.event
async def on_ready():
    global connected
    if not connected:
        connected = True
        global channel
        channel = await bot.fetch_channel(channel_id)

@bot.event
async def on_message(message):
    global outgoing_msgs
    if check_timeout():
        if (message.channel.id == channel_id) and (message.author.id != bot.user.id):
            msg = {
                'author': message.author.name if not do_use_nicknames else message.author.display_name,
                'content': message.content.replace('\n', '/')
            }
            if do_clean_invites:
                msg['content'] = clean_invites(msg['content'])
            if msg['content'] != '':
                # TODO: Delete offensive message
                if await check_violation(message.content):
                    await transmit_to_filtershook("[discordmt] Message blocked from Discord: Author: `" + str(message.author.name) + "` - Nick: `" + str(message.author.display_name) + "` -  Msg: `" +  message.content.replace('\n', '/') + "`")
                    logger.warning("[discordmt] Message blocked from Discord")
                else:
                    outgoing_msgs.add(msg)
    await bot.process_commands(message)

@bot.command(help='Runs an ingame command from Discord.')
async def cmd(ctx, command, *, args=''):
    await transmit_to_bothook('[discordmt] Command executed by:' + str(ctx.author.id) + ' - ' + str(ctx.author.name) + ' Parameters: ' + str(command) + ' ' + str(args))
    if not await check_violation(command + ' ' + args):
        #if not check_timeout():
        #    logger.warning("[discordmt] Server Down cannot execute command: " + str(command))
        #    await transmit_to_bothook("[discordmt] The server currently appears to be down.")
        #    await ctx.send("[discordmt] The server currently appears to be down.")
        #    return
        if ((ctx.channel.id != channel_id) and ctx.guild is not None) or not logins_allowed:
            logger.warning("[discordmt] Login Disallowed")
            await transmit_to_bothook("[discordmt] Login Disallowed")
            return
        if ctx.author.id not in authenticated_users.keys():
            logger.warning("[discordmt] Not logged in")
            await transmit_to_bothook("[discordmt] User :" + str(ctx.author.name) + " - Not logged in to execute command: " + str(command) + "`")
            await ctx.send('[discordmt] User Not logged in.')
            return
        command = {
            'name': authenticated_users[ctx.author.id],
            'command': command,
            'params': args.replace('\n', '')
        }
        if ctx.guild is None:
            command['context'] = str(ctx.author.id)
        command_queue.add(command)
    else:
        await transmit_to_filtershook("[discordmt] Violation of `" + str(ctx.author.name) + "` in command: `" + str(command) + "`")
        return
    
@bot.command(help='Logs into your ingame account from Discord so you can run commands. You should only run this command in DMs with the bot.')
async def login(ctx, username, password=''):
    if not logins_allowed:
        logger.warning("[discordmt] Login Disallowed")
        return
    if ctx.guild is not None:
        logger.warn('[discordmt] Password Leak!')
        await transmit_to_bothook("**Discord Bridge Probable Password Leak!**: Discord UserID `" +  str(ctx.author.id) + 
            " - " + str(ctx.author.name) + "` - Ingame User: `" + str(username) + '`')
        await ctx.send(ctx.author.mention+' You\'ve quite possibly just leaked your password by using this command outside of DMs; it is advised that you change it at once.\n*This message will be automatically deleted.*', delete_after = 10)
        try:
            await ctx.message.delete()
        except:
            print(f"Unable to delete possible password leak by user ID {ctx.author.id} due to insufficient permissions.")
        return

    login_queue.add({
        'username' : username,
        'password' : password,
        'user_id' : str(ctx.author.id)
    })
    if not check_timeout():
        logger.warning('[discordmt] Login failed')
        await transmit_to_bothook("**Discord Bridge Login Failed**: The server currently appears to be down, but your login attempt has been added to the queue", webhook_bot)
        await ctx.send("The server currently appears to be down, but your login attempt has been added to the queue and will be executed as soon as the server returns.")

@bot.command(help='Lists connected players and server information.')
async def status(ctx, *, args=None):
    if not check_timeout():
        logger.warning("[discordmt] Server Down ")
        await ctx.send("The server currently appears to be down.")
        return
    if ((ctx.channel.id != channel_id) and ctx.guild is not None):
        return
    data = {
        'name': 'discord_relay',
        'command': 'status',
        'params': '',
    }
    if ctx.guild is None:
        data['context'] = str(ctx.author.id)
    command_queue.add(data)

async def runServer():
    runner = web.AppRunner(app)
    await runner.setup()
    site = web.TCPSite(runner, listen, port)
    await site.start()

async def runBot():
    await bot.login(token)
    await bot.connect()

try:
    print('='*37+'\nStarting relay. Press Ctrl-C to exit.\n'+'='*37)
    loop = asyncio.get_event_loop()
    futures = asyncio.gather(runBot(), runServer())
    loop.run_until_complete(futures)

except (KeyboardInterrupt, SystemExit):
    sys.exit()